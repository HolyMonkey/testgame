<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;
use UserOnGame;

class Loby extends Model {
    
	public function users()
    {
        return $this->belongsTo('App\UserOnGames', 'loby_id');
    }
    
    public function getStatusAttribute(){
        if($this->end_date == "0000-00-00 00:00:00")
        {
            if($loby_joined_count = UserOnGames::where('loby_id', '=', $this->id)->count() == $this->user_count){
               return Lang::get("app.state_full");
            }else{
               return Lang::get("app.state_stay");
            }
        }
        else{
            return Lang::get("app.state_run");
        }
    }
    
    public function getIsStartedAttribute(){
        return $this->start_date != "0000-00-00 00:00:00";
    }
}
