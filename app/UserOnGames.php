<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserOnGames extends Model {

	public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
	
	public function loby()
	{
		return $this->belongsTo('App\Loby', 'loby_id');
	}
}
