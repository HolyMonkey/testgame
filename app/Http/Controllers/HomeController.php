<?php namespace App\Http\Controllers;

use App\OnlineUser;
use App\Loby;
use App\UserOnGames;
use Auth;
use Carbon\Carbon;
use App\LobyMessage;
use DB;
use Request;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
        $registered = OnlineUser::where('last_activity', '>', Carbon::now()->subMinute(5)->timestamp)->get();      
		$lobies = Loby::where('end_date', '>',  Carbon::now())->orWhere('end_date', '=', '0000-00-00 00:00:00');
        $lobies = Loby::where(function($query){
            $query->where('end_date', '>',  Carbon::now())->orWhere('end_date', '=', '0000-00-00 00:00:00');
        });
        
        
		$lobies = $lobies->get();
        $lobies2 = array();
        $lobies4 = array();
        
		foreach($lobies as $loby)
        {
            if($loby->user_count == 2)
                array_push($lobies2, $loby);
            else
                array_push($lobies4, $loby);
        }
		
        
		$current = UserOnGames::where('user_id', '=', Auth::user()->id)->orderBy('id', 'desc')->first();
		$current_id = 0;
		if($current && ($current->loby->start_date == "0000-00-00 00:00:00" || $current->loby->end_date > Carbon::now()))
			$current_id = $current->loby_id;
		
        $isUserMax = false;
        if(Request::has('error'))
            $isUserMax = true;
        
        $isBaned = Auth::user()->ban_end_date > Carbon::now();
        $timeToEndBan = 0;
        
        if($isBaned)
        {
            
            $now = Carbon::now();
			$end_date = Auth::user()->ban_end_date;
					
			$diff = $now->diff(Carbon::parse($end_date));
            
            $timeToEndBan = (($diff->i * 60) + $diff->s);
        }
		return view('home', ['online' => $registered, 
                             'lobies2' => $lobies2, 
                             'lobies4' => $lobies4,
							 'current_loby' => $current_id, 
                             'isBaned' =>  $isBaned, 
                             'timeToEndBan' => $timeToEndBan , 
                             'last_id' => DB::table('loby_messages')->orderBy('id', 'desc')->first()->id, 
                             'isUserMax' => $isUserMax]);

	}
	
	public function loby()
	{
		return view('loby');
	}

}
