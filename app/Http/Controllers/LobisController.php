<?php namespace App\Http\Controllers;

use Session;
use App\Loby;
use App\User;
use App\UserOnGames;
use App\LobyMessage;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use DB;

class LobisController extends Controller {

	public function loby(Request $request)
    {	
        $user = Auth::user();
		$uog = $user->currentGame;
		if(empty($uog))
			return redirect ('/');
		$loby = $uog->loby;
        
 		if(!$loby->is_end)
        {
            $users = UserOnGames::where('loby_id', '=', $loby->id)->get();
			$isJoined = false;
            
			for($i = 0; $i < count($users); $i++)
            {
				$users[$i] = $users[$i]->user;
				if($user->id == $users[$i]->id)
                {
					$isJoined = true;
                }
			}
            
			return view('loby', ['loby' => $loby, 
                                 'users' => $users, 
                                 'isJoined' => $isJoined, 
                                 'isStarted' => $loby->isStarted, 
                                 'last_id' => DB::table('loby_messages')->orderBy('id', 'desc')->first()->id]);
		}
        else
        {
			return redirect ('/');
		}
	}
	
	public function create(Request $request)
    {
		$user = Auth::user();
		$uog = $user->currentGame;
        
		if(!$user->isBaned)
        {
			$user_count = $request->input('user_count');
			if($user_count != 2 && $user_count != 4)
				$user_count = 2;
			
			$loby = new Loby;
			$loby->host_id = $user->id;
			$loby->name = empty($request->input('name')) ? "Simple Loby" : $request->input('name');
			$loby->user_count = $user_count;
			$loby->is_end = false;
			$loby->save();
			
            $this->pull_message('OnCreate', ['loby_id' => $loby->id, 
                                             'loby_name' => $loby->name, 
                                             'user_count' => $user_count], $loby->id);
			$this->join_loby($loby->id);
			
			return redirect("/loby");
		}
        else
        {
            return redirect("/");
        }
	}
	
	public function start()
    {
        $user = Auth::user();
		$loby = $user->currentGame->loby;
		$uog = UserOnGames::where('loby_id', '=',$loby->id)->count();
        
		if($uog == $loby->user_count && !$loby->is_end)
        {
			$start_date = Carbon::now();
			$end_date = Carbon::now()->addMinute(1);
			$loby->start_date = $start_date;
			$loby->end_date = $end_date;
			$loby->save();
			
					
			$diff = $start_date->diff($end_date);
			
            $this->pull_message('OnStart', ['time_to_end' => (($diff->i * 60) + $diff->s)], $loby->id);
		}
		
		return redirect("/loby");
	}
	
	public function join_loby($id)
    {
		$loby = Loby::find($id);
		$user = Auth::user();
		$last_loby = $user->currentGame;
        
		if($loby)
        {	
			$loby_joined_count = UserOnGames::where('loby_id', '=', $loby->id)->count();
			
			if($last_loby)
            {
				$last_loby = $last_loby->loby;
				if($last_loby->start_date == "0000-00-00 00:00:00")
                {
					return redirect("/");
				}
                else
                {
					if($last_loby->end_date > Carbon::now())
                    {
						return redirect("/");
					}
				}
			}
			
			if(!Auth::user()->isBaned && $loby->user_count > $loby_joined_count)
            {	
				$uog = new UserOnGames;
				$user = Auth::user();
				$uog->user_id = $user->id;
				$uog->loby_id = $id;
				
				$uog->save();
				
                $this->pull_message('OnJoin', ['user_id' => 
                                               $user->id, 'user_name' => $user->name, 
                                               'isFully' => $loby->user_count == ($loby_joined_count + 1)], $id);
                
                if($loby_joined_count == 0){
                    $loby->host_id = $user->id;
                    $loby->save();
                }
                
				return redirect("/loby");
			}
            else
            {
                return redirect("/?error=user_max");
            }
		}
		return redirect("/");
	}
	
	public function exit_loby()
    {
		$user = Auth::user();
		$scope = $user->currentGame;
		$loby = $scope->loby;
        
		if($loby->isStarted && !$loby->is_end)
        {
			$user->ban_end_date = Carbon::now()->addMinute(1);
		}

        $this->pull_message('OnExit', ['user_id' => $user->id, 
                                       'isLobyStart' => $loby->isStarted], $loby->id);
		
		$user->save();
		$scope->delete();
		
        if($loby->host_id == $user->id)
        {
            $last_user = UserOnGames::where('loby_id', '=', $loby->id)->first();
            
            if($last_user)
            {
                $loby->host_id = $last_user->user_id;
                $loby->save();
            }
        }
        
        
		return redirect("/");
	}
	
    public function comet()
    {
        $limit = 360;
        $time = time();
        $last_id = (int)$_GET['id'];
        $user = Auth::user();
        $scope = $user->currentGame;
        $loby = null;
        if($scope)
            $loby = $scope->loby;
        
        set_time_limit($limit+5);

        while (time()-$time<$limit)
        {
            $messages = LobyMessage::where('id', '>', $last_id)->orderBy('id', 'asc')->get();
            
            if (count($messages)) 
            {
                $out = array();
                
                foreach($messages as $message)
                    $out[$message->id] = $message->message;
                
                echo json_encode($out);			
                flush();
                exit;
            }
            if($loby && !$loby->is_end && $loby->start_date != "0000-00-00 00:00:00" && Carbon::now() > $loby->end_date)
            {
                $this->pull_message('OnEnd', ['loby_id' => $loby->id], $loby->id);
                $loby->is_end = true;
                $loby->save();
            }
            
            sleep(1);
        }
    }
	
    
    public function pull_message($name, $data, $loby_id = 0)
    {
        $loby_message = array();
        $loby_message['name'] = $name;
        $loby_message['data'] = $data;
        $loby_message['loby_id'] = $loby_id;
        
        if($loby_id != 0)
        {
            $loby = Loby::find($loby_id);
            
            if($loby)
            {
                $loby_message['host_id'] = $loby->host_id;
            }
        }                
        
        $lm = new LobyMessage;
        $lm->loby_id = $loby_id;
        $lm->message = json_encode($loby_message);
        $lm->save();
    }
}