<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
	App::setLocale(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2));


Route::get('/', 'HomeController@index');
Route::group(['middleware' => ['auth']], function()
{
	Route::get('home', 'HomeController@index');

	Route::get('loby', 'LobisController@loby');
	Route::post('loby/create', 'LobisController@create');
	Route::get('loby/{id}/join', 'LobisController@join_loby');
	Route::get('loby/exit', 'LobisController@exit_loby');
	Route::get('loby/start', 'LobisController@start');
	Route::get('match', 'LobisController@comet');
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


