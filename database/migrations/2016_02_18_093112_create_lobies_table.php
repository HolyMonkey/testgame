<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLobiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lobies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('host_id');
			$table->integer('user_count');
			$table->char(256);
			$table->boolean('is_end');
			$table->timestamp('start_date');
			$table->timestamp('end_date');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lobies');
	}

}
