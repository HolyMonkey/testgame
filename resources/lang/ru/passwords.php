<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Пароль должен быть не меньше 6 символов и они должны совпадать.",
	"user" => "Мы не можем найти пользователя с таким email адресом.",
	"token" => "Этот ключ востановления пароля не подходит.",
	"sent" => "Мы отправили вам на email ссылку на востановление пароля!",
	"reset" => "Ваш пароль сброшен!",

];
