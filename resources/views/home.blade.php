@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			@if($current_loby != 0)
				<div class="alert alert-success" role="alert"><a href="/loby"> {{ Lang::get('app.you_loby') }} </a></div>
			@endif
			@if($isBaned)
				<div id="banned" class="alert alert-danger" role="alert" onload="">{{ Lang::get('app.baned') }}</div>
                <script>
                    setInterval('$("#banned").hide()', {{ $timeToEndBan }} * 1000); 
                </script>
			@endif
            @if ($isUserMax)
                <div class="alert alert-danger">
                    {{ Lang::get('app.user_max') }}
                </div>
            @endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-10 col-md-offset-1" style="margin-top:15px; margin-bottom:15px;">
			<center>
				<form action="/loby/create" method="POST" class="form-inline">
					<input name="name" class="form-control input-lg" type="text" required="required" placeholder="{{ Lang::get("app.create-placeholder-1") }}">
					<select name="user_count" class="form-control input-lg">
						<option value="2">{{ Lang::get("app.create-var-1") }}</option>
						<option value="4">{{ Lang::get("app.create-var-2") }}</option>
					</select>
					<button type="submit" class="btn btn-lg btn-success">{{ Lang::get("app.create-submit") }}</button>
					
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
				</form>
			</center>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">{{ Lang::get("app.games") }}</div>

				<div class="panel-body">
					<div class="col-md-6">
						<table class="table table-hover">
						  <thead> 
							<tr> 
								<th>{{ Lang::get("app.create-var-1") }}</th>
							</tr> 
						  </thead>
						  
						  <tbody id="two_players_lobys"> 
							@foreach($lobies2 as $loby)
								<tr id="{{ $loby->id }}" style="cursor: pointer;" onclick="if(confirm('{{ Lang::get("app.confirm") }}'))location.href='/loby/{{$loby->id}}/join'"> 
                                    <th scope="row">{{ $loby->name }} <br><p class='status'>{{ $loby->status }}</p></th>
								</tr> 
							@endforeach
						  </tbody>
						</table>
					</div>
					<div class="col-md-6">
						<table class="table table-hover">
						  <thead> 
							<tr> 
								<th>{{ Lang::get("app.create-var-2") }}</th> 
							</tr> 
						  </thead>
						  
						  <tbody id="four_players_lobys"> 
							@foreach($lobies4 as $loby)
								<tr id="{{ $loby->id }}" style="cursor: pointer;" onclick="if(confirm('{{ Lang::get("app.confirm") }}'))location.href='/loby/{{$loby->id}}/join'"> 
									<th scope="row">{{ $loby->name }} <br><p class='status'>{{ $loby->status }}</p></th>
								</tr> 
							@endforeach
						  </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">{{ Lang::get("app.players-online") }} ({{ count($online)  }})</div>

				<div class="panel-body">
					@foreach($online as $user)
						{{ $user->user['name'] }} |
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
    <script>
        Messanger({{ $last_id }}, 'home', {{ Auth::user()->id }});
    </script>
@endsection