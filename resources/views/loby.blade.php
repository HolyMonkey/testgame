@extends('app')

@section('content')
<div class="container" style="text-align:center;">
	<div class="row">
		<div class="col-md-12">
			<h1>{{ $loby->name }}</h1>
		</div>
	</div>
	<div class="row">
        @for ($i = 0; $i < $loby->user_count; $i++)
            <div class="col-md-3">
                <div data-seat="1" data-user="@if(isset($users[$i])){{$users[$i]->id}}@else{{0}}@endif">
                    @if(isset($users[$i] ))
                        {{ $users[$i]->name }}
                    @else
                        {{ Lang::get("app.free_seat") }}
                    @endif
                </div>
            </div>
        @endfor
	</div>
	<div class="row">
		<div class="col-md-12">
			<center><a href="/loby/start"> <button id="startButton" class="btn btn-lg btn-success" style="display:none">{{ Lang::get("app.start") }}</button> </a></center>
			
			@if($isJoined)
				<center><a href="/loby/exit"> <button class="btn btn-lg btn-success">{{ Lang::get("app.exit") }}</button> </a></center>
			@elseif(!$isJoined && !$isStarted)
				<center><button class="btn btn-lg btn-success">{{ Lang::get("app.join") }}</button></center>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h3 id="time"> </h3>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
    @if($isStarted)
		<? 
			$diff = Carbon\Carbon::parse($loby->end_date)->diff(Carbon\Carbon::now());
		?>
		table['loby'].OnStart({ time_to_end: {{ (($diff->i * 60) + $diff->s) }} });
	@endif
	
	@if(Auth::user()->id == $loby->host_id && count($users) == $loby->user_count && !$isStarted) 
		$("#startButton").show(1000);		
	@endif
    Messanger({{ $last_id }}, 'loby', {{ Auth::user()->id }}, {{ $loby->id }});
</script>
@endsection