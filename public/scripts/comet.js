this.table = {
    home: {
        OnCreate: function (data) {
            tr = $('<tr/>', {
                style: "cursor: pointer;",
                onclick:"if(confirm('"+LANG['confirm']+"'))location.href='/loby/"+ data['loby_id'] +"/join'",
                id:data['loby_id']
            });
            th = $('<th/>', {
                scope:"row"
            });
            $(th).html(data['loby_name'] + "<br><p class='status'>"+LANG['state_stay']+"</p>");
            tr.append(th);
            
            if(data['user_count'] == 2)
                $("#two_players_lobys").append(tr);
            else
                $("#four_players_lobys").append(tr);
        },
        OnJoin: function (data) {
            if(data['isFully']){
                $("#"+data['loby_id']).children('th').children('.status').text(LANG['state_full']);
            }
        },
        OnExit: function (data) {
            if(!data['isLobyStart'])
                $("#"+data['loby_id']).children('th').children('.status').text(LANG['state_stay']);
        },
        OnEnd: function (data) {
            $("#"+data['loby_id']).remove();
        },
        OnStart: function (data) {
             $("#"+data['loby_id']).children('th').children('.status').text(LANG['state_run']);
        }
    },
    loby: {
        OnCreate: function (data) {
            "use strict";
        },
        OnJoin: function (data) {
            seat = $("[data-user=0]");
            seat.attr('data-user', data['user_id']);
            seat.text(data['user_name']);
            if(data['isAdmin']){
                if($("[data-user=0]").length == 0)
                    $("#startButton").show(1000);
            }
            console.log(data);
        },
        OnExit: function (data) {
            seat = $("[data-user="+data['user_id']+"]");
            seat.attr('data-user', 0);
            seat.text(LANG['free_seat']);
            
            if(data['isAdmin']){
                 $("#startButton").hide(1000);
            }
        },
        OnEnd: function (data) {
            location.href = "/";
        },
        OnStart: function (data) {
            setInterval('timer()',1000);
            time = data['time_to_end'];
            if(data['isAdmin']){
                 $("#startButton").hide(1000);
            }
        }
    }
};


function Messanger(last_id, page, user_id, loby_id) {
    this.last = last_id;
    this.user_id = user_id
    this.loby_id = loby_id == undefined ? 0 : loby_id;
    this.timeout = 360;
    this.comet = 0;
    this.event_table = this.table[page];
    var self = this;
    this.parseData = function(message) {
        
        var data = $.parseJSON(message);
        if (data.length<1) return false;
        console.log(data);
        $.each(data, function(index, value){
            self.last = index;
            reciev_data = $.parseJSON(value);
            if(page != 'loby' || reciev_data['loby_id'] == loby_id){
                reciev_data['data']['isAdmin'] = user_id == reciev_data['host_id'];
                reciev_data['data']['loby_id'] =  reciev_data['loby_id'];
                reciev_data['data']['name'] =  reciev_data['name'];
                self.event_table[reciev_data['name']](reciev_data['data']);
            }
        });
        
        setTimeout(self.connection,1000);
    }
    this.connection = function() {
        self.comet = $.ajax({
                type: "GET",
                url:  "match",
                data: {'id':self.last},
                dataType: "text",
                timeout: self.timeout*1000,
                success: self.parseData,
                error: function(){
                    setTimeout(self.connection,1000);
               }
            });
    }
    this.init = function() {
        //setInterval(self.connection,self.timeout*1000);
        self.connection();
    }
    this.init();
}

var time = 60;
function timer(end){
    if(time > 0)
        document.getElementById('time').innerHTML = time--;
}